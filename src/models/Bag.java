package models;
import java.util.LinkedHashSet;

/**
 * 
 */

/**
 * @author vaibhavsaini
 * 
 */
public class Bag extends LinkedHashSet<TokenFrequency> {
    /**
     * 
     */
    private static final long serialVersionUID = 1721183896451527542L;
    public long id;
    public int size;
    public int comparisions;

    /**
     * @param bagId
     */
    public Bag(long bagId) {
        super();
        this.id = bagId;
        this.size =0;
        this.comparisions=0;
    }
    
    public Bag(){
        super();
    }

    /**
     * @return the id
     */
    public long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String returnString = "";
        for (TokenFrequency tokenFrequency : this) {
            returnString += tokenFrequency.getToken().toString() + "@@::@@"
                    + tokenFrequency.getFrequency() + ",";
        }
        return this.id+ "@#@"+ returnString.substring(0,returnString.length()-1) + System.getProperty("line.separator");
    }

    
    public TokenFrequency get(TokenFrequency tokenFrequency) {
        this.comparisions=0;
        for (TokenFrequency tf : this) {
            this.comparisions+=1;
            if (tf.equals(tokenFrequency)) {
                return tf;
            }
        }
        return null;
    }
    

    public int getSize() {
        if(this.size == 0){
        	for (TokenFrequency tf : this) {
                this.size += tf.getFrequency();
            }
        }
        return this.size;
    }
}
